var fs = require('fs');

this.readFile = function(srcPath, callback) {
	return fs.readFile(srcPath, 'utf8', function (err, data) {
        if (err) 
			throw err;
        return callback(data);
    });
}

this.writeFile = function(savPath, data) {
	fs.writeFile(savPath, data, function(err) {
		if (err)
			throw err;
		console.log("written");
	});
}