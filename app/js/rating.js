var table_data = [
	{
		id:'1',
		nick:'Andrew',
		points:'10'
	},
	{
		id:'2',
		nick:'Lina',
		points:'9'
	},
	{
		id:'3',
		nick:'Ivan',
		points:'7'
	}
]
fill_table('rating_table', table_data);
function fill_table(table_class, table_data){
	var tableBody = document.getElementsByClassName(table_class)[0];
	tableBody.innerHTML = '';
	
	for(var i = 0; i < table_data.length; i++){
		var tr = document.createElement('tr');
		for(var key in table_data[i]){
			var td = document.createElement('td');
			td.innerHTML = table_data[i][key];
			tr.appendChild(td);
		}
		tableBody.appendChild(tr);
	}
}