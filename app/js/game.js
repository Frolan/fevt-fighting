var cardsSelected = [];
var cardsArr = [];
function clickOnCard(card){
    if(card.classList.contains("card-selected")){
        card.classList.remove("card-selected")
    }
    else{
        // Сначала проверяем, что выделено не больше 5 карт
        if(selectedCardsCount() < 5){
            card.classList.add("card-selected");
        }
        else{
            alert("Нельзя выбрать больше 5 карт!")
        }
    }  
}

function selectedCardsCount(){
    cardsSelected = document.querySelectorAll(".card-selected");
    return cardsSelected.length;
}

$('.createDec_btn').click(function(){
    if(selectedCardsCount() == 5){
        // Получаем айдишники выбранных карт
        cardsSelected = document.querySelectorAll(".card-selected");
        var cardsSelectedInfo = [];
        cardsSelected.forEach(function(card) {
            cardsArr.forEach(function(cardArr){
                if(card.id == cardArr['card_id']){
                    cardsSelectedInfo.push(cardArr);
                }
            });
        });
        console.log(cardsSelectedInfo);

        // Отправляем запрос
        $.ajax({
            url: "/start_game",
            type: "POST",
            data: {
                "selectedCards": cardsSelectedInfo
            },
            success: function(res) {  
                // здесь надо что-то сделать
            },
            error : function() {
                alert("Ошибка доступа к серверу!")
            }
        });

    }
    else{
        alert("Выберите 5 карт для колоды перед началом игры!")
    }
});

function getCards(){
    console.log('запрос на получение карт отправлен');
    $.ajax({
        url: "/game",
        type: "GET",
        success: function(cards) {  
            addCardsToHTML(cards);
            cardsArr = cards;
        },
        error : function() {
            alert("Ошибка доступа к серверу!")
        }
    });
}

function removeChildren(elem) {
    while (elem.lastChild) {
      elem.removeChild(elem.lastChild);
    }
}

function addCardsToHTML(cards){
    removeChildren(document.getElementById("cards-container"));
    cards.forEach(function(card) {
        var img = document.createElement("img");
        img.setAttribute("src", card['path']);
        img.setAttribute("class", "class");
        img.setAttribute("id", card['card_id']);
        img.setAttribute("onclick", "return clickOnCard(this)");
        document.getElementById("cards-container").appendChild(img);
    });
    
}