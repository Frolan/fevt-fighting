var app=angular.module('app',['ngRoute','ngSanitize']);

app.config(function($routeProvider){
  $routeProvider
  .when('/',{
    templateUrl : 'html/about.html'
  })
  .when('/authorization',{
    templateUrl : 'html/authorization.html'
  })
  .when('/registration',{
    templateUrl : 'html/registration.html'
  })
  .when('/rating',{
    templateUrl : 'html/rating.html'
  })
  .when('/game', {
    templateUrl : 'html/game.html', 
    controller: LoginController
  })
  .otherwise({redirectTo: 'authorization'});
});